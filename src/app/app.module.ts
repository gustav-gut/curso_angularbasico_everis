import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';
import { HttpClientModule } from '@angular/common/http';
import { UsersComponent } from './components/users/users.component';
import { UsersphService } from './services/usersph.service';
import { ToUpercasePipe } from './pipes/to-upercase.pipe';
import { PipePipe } from './pipe.pipe';
import { PipePruebaPipe } from './pipe-prueba.pipe';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    AboutComponent,
    UsersComponent,
    ToUpercasePipe,
    PipePipe,
    PipePruebaPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [ 
    UsersphService 
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
