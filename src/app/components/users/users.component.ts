import { Component, OnInit } from '@angular/core';
import { UsersphService } from 'src/app/services/usersph.service';
import { Users } from './models/users';
import { Persons } from './models/persons';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users : Users[];
  show: boolean;
  people: Persons[];

  constructor(
    private usersServices:UsersphService) { }

  ngOnInit() {

    this.usersServices.getUsers().subscribe(data => {
      this.users = data;
      this.show = true;
      console.log(this.users);
    });

    this.usersServices.getPersonSW().subscribe(data => {
      this.people = data,
      this.show = true;
      console.log(this.people);
    });

  }

}
