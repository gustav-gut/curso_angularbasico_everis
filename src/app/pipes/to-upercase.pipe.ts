import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'toUpercase'
})
export class ToUpercasePipe implements PipeTransform {

  transform(text : string): string {
    return text.toUpperCase();
  }

}
